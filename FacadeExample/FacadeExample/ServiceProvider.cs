﻿using FacadeExample.DataProviders;
using FacadeExample.Services;

namespace FacadeExample
{
    public static class ServiceProvider
    {
        public static ValidationService GetValidationService() => new ValidationService();
        public static UserService GetUserManager() => new UserService(GetDataService());
        public static SignInService  GetSignInManager() => new SignInService(GetDataService());
        public static OrderService GetOrderService() => new OrderService(GetDataService());

        private static IDataService GetDataService() => new XmlDataService();

        
    }
}