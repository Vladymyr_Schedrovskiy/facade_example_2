﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacadeExample.Models;

namespace FacadeExample.DataProviders
{
    public interface IDataService
    {
        User FindUser(Func<User, bool> predicate);
        Order FindOrder(Func<Order, bool> predicate);
        List<User> GetUsers(Func<User, bool> predicate);
        List<Order> GetOrders(Func<Order, bool> predicate);
        void SaveUser(User user);
        void SaveOrder(Order order);
        void DeleteUser(User user);
        void DeleteOrder(Order order);
    }
}
