﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacadeExample.DataProviders;
using FacadeExample.Models;
using FacadeExample.Services;

namespace FacadeExample.Forms
{
    public partial class OrderForm : Form
    {
        public string UserName { get; private set; }
        private readonly UserService _userService;
        private readonly OrderService orderService;
        private readonly ValidationService validationService;
        public OrderForm(User user)
        {
            UserName = user.Login;
            _userService = ServiceProvider.GetUserManager();
            orderService = ServiceProvider.GetOrderService();
            validationService = ServiceProvider.GetValidationService();
            InitializeComponent();
        }

        private void orderAddButton_Click(object sender, EventArgs e)
        {
            try
            {
                string orderName = orderNameInput.Text.Trim();
                bool modelState = validationService.ValidateOrderForm(orderName, this);
                if (modelState)
                {
                    User user = _userService.FindUserByLogin(UserName);
                    Order order = orderService.CreateOrder(orderName, user);
                    orderService.AddOrder(order);
                    orderNameInput.ResetText();
                    MessageBox.Show("Order was added.");
                }
               
            }
            catch (ApplicationException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void logOutLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            this.Hide();
            loginForm.ShowDialog();
            this.Dispose();
        }
    }
}
