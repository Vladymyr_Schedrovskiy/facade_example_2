﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacadeExample.DataProviders;
using FacadeExample.Forms;
using FacadeExample.Models;
using FacadeExample.Services;

namespace FacadeExample
{
    public partial class LoginForm : Form
    {
        private readonly ValidationService validationService;
        private readonly SignInService _signInService;
        private readonly UserService _userService;
        public LoginForm()
        {
            _userService = ServiceProvider.GetUserManager();
            _signInService = ServiceProvider.GetSignInManager();
            validationService = ServiceProvider.GetValidationService();
            InitializeComponent();
            
        }

        private void sign_in_button_Click(object sender, EventArgs e)
        {
            try
            {
                string login = login_input.Text.Trim();
                string password = password_input.Text.Trim();
                

                bool modelState = validationService.ValidateLoginForm(login, password,  this);
                if (modelState)
                {
                    User user = _userService.FindUserByLogin(login);
                    if (user == null)
                    {
                        throw new ApplicationException("User with such login does not exist!");
                    }
                    _signInService.PasswordSignIn(login, password);
                    OrderForm orderForm = new OrderForm(user);
                    this.Hide();
                    orderForm.ShowDialog();
                    this.Dispose();
                }
            }
            catch (ApplicationException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void register_link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RegisterForm form = new RegisterForm();
            this.Hide();
            form.ShowDialog();
            this.Dispose();
        }

        private void password_input_TextChanged(object sender, EventArgs e)
        {
            password_validation.ResetText();
        }
    }
}
