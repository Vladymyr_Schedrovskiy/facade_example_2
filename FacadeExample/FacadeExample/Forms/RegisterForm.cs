﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacadeExample.DataProviders;
using FacadeExample.Models;
using FacadeExample.Services;

namespace FacadeExample.Forms
{
    public partial class RegisterForm : Form
    {
        private readonly ValidationService validationService;
        private readonly UserService _userService;
        private readonly SignInService _signInService;
        public RegisterForm()
        {
            validationService = ServiceProvider.GetValidationService();
            _userService = ServiceProvider.GetUserManager();
            _signInService = ServiceProvider.GetSignInManager();
            InitializeComponent();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            try
            {
                string login = login_input.Text.Trim();
                string password = password_input.Text.Trim();
                string confirmPassword = confirm_input.Text.Trim();

                bool modelState = validationService.ValidateRegisterForm(login, password, confirmPassword, this);
                if (modelState)
                {
                    User user = _userService.CreateUser(login, password);
                    _userService.AddUser(user);
                    _signInService.PasswordSignIn(login, password);
                    OrderForm orderForm = new OrderForm(user);
                    this.Hide();
                    orderForm.ShowDialog();
                    this.Dispose();
                }
            }
            catch (ApplicationException exception)
            {
                MessageBox.Show(exception.Message);
            }
            

        }

        private void SignInLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LoginForm form = new LoginForm();
            this.Hide();
            form.ShowDialog();
            this.Dispose();
        }
    }
}
