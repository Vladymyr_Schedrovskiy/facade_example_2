﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacadeExample.Models
{
    public class Order
    {
        [Key]
        public string Id { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public string OrderName { get; set; }
    }
}
