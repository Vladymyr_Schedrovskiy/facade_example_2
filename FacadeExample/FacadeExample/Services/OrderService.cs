﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacadeExample.DataProviders;
using FacadeExample.Models;

namespace FacadeExample.Services
{
    public class OrderService
    {
        private readonly IDataService dataService;

        public OrderService(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public Order CreateOrder(string orderName, User user)
        {
            Order order = new Order{OrderName = orderName, User = user, UserId = user.Id};
            return order;

        }

        public void AddOrder(Order order)
        {
            if (string.IsNullOrEmpty(order.UserId) && order.User == null)
            {
                throw new ApplicationException("Can not add an order without a user!");
            }
            dataService.SaveOrder(order);
        }

        public List<Order> GetUserOrders(User user) => dataService.GetOrders(o => o.UserId == user.Id);

        
        
    }
}
