﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacadeExample.DataProviders;
using FacadeExample.Models;

namespace FacadeExample.Services
{
    public class UserService
    {
        private readonly IDataService dataService;

        public UserService(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public User CreateUser(string login, string password)
        {
            User user = new User{Login = login, Password = password};
            return user;
        }

        public void AddUser(User user)
        {
            if (dataService.FindUser(u => u.Login == user.Login) != null)
            {
                throw new ApplicationException("User with such login already exists!");
            }
            else
            {
                dataService.SaveUser(user);
            }
        }

        public User FindUserByIdAsync(string userId)
        {
            User user =  dataService.FindUser(u=>u.Id == userId);
            return user;
        }

        public User FindUserByLogin(string login)
        {
            User user = dataService.FindUser(u => u.Login == login);
            return user;
        }

        public void RemoveUser(string userId)
        {
            User user = dataService.FindUser(u => u.Id == userId);
            dataService.DeleteUser(user);
        }
    }
}
