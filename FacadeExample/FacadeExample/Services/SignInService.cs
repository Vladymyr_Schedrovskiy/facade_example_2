﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacadeExample.DataProviders;
using FacadeExample.Models;

namespace FacadeExample.Services
{
    public class SignInService
    {
        private readonly IDataService dataService;

        public SignInService(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public void PasswordSignIn(string login, string password)
        {
            User user = dataService.FindUser(u => u.Login == login);
            if(user == null)  throw new ApplicationException("Could not sign in: user not found!");
            if(!CheckPassword(user, password)) throw new ApplicationException("Could not sign in: wrong login or password!");
        }

        public bool CheckPassword(User user, string password) => user.Password == password;
        
    }
}
