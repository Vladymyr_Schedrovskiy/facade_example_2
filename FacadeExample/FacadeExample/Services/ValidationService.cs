﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacadeExample.Forms;

namespace FacadeExample.Services
{
    public class ValidationService
    {
        public bool ValidateRegisterForm(string login, string password, string confirmPassword, RegisterForm form)
        {
            bool modelState = true;
            if (string.IsNullOrEmpty(login))
            {
                form.login_validation.Text = "*Enter login!";
                modelState = false;
            }
           

            if (string.IsNullOrEmpty(password))
            {
                form.password_validation.Text = "*Enter password!";
                modelState = false;
            }

            if (password != confirmPassword)
            {
                form.confirm_validation.Text = "*Passwords do not match!";
                modelState = false;
            }
            return modelState;
        }

        public bool ValidateLoginForm(string login, string password, LoginForm form)
        {
            bool modelState = true;
            if (string.IsNullOrEmpty(login))
            {
                form.login_validation.Text = "*Enter login!";
                modelState = false;
            }
            if (string.IsNullOrEmpty(password))
            {
                form.password_validation.Text = "*Enter password!";
                modelState = false;
            }
          return modelState;
        }

        public bool ValidateOrderForm(string orderName, OrderForm form)
        {
            bool modelState = true;
            if (string.IsNullOrEmpty(orderName))
            {
                modelState = false;
                form.orderNameValidation.Text = "*Enter order name!";

            }
            return modelState;
        }

    }
}
